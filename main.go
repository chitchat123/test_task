package main

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
)

// User Data types
//
type User struct {
	ID       primitive.ObjectID `bson:"_id" json:"id"`
	Username string             `json:"username" binding:"required"`
	Password string             `json:"password" binding:"required"`
}

type Coupon struct {
	Title       string `json:"title" binding:"required"`
	Document    string `json:"doc" binding:"required"`
	Description string `json:"description" binding:"required"`
	CouponCode  string `json:"coupon_code" binding:"required"`
}

type Session struct {
	UserID primitive.ObjectID `json:"id,omitempty"`
	Token  string             `binding:"required"`
}

// DB Database interaction
//
type DB struct {
	client *mongo.Client
}

func (db *DB) register(ctx context.Context, user User) error {
	var res User
	if err := db.client.Database("coding_challenge").Collection("users").FindOne(ctx, bson.M{"username": user.Username}).Decode(&res); err != nil {
		//return err
	}
	if res.Username == user.Username {
		return errors.New("user already exist")
	}
	user.ID = primitive.NewObjectID()
	_, err := db.client.Database("coding_challenge").Collection("users").InsertOne(ctx, user)
	return err
}

func (db *DB) login(ctx context.Context, user User, token string) (Session, error) {
	var res User
	var userSession Session
	if err := db.client.Database("coding_challenge").Collection("users").FindOne(ctx, bson.M{"username": user.Username}).Decode(&res); err != nil {
		return Session{}, err
	}
	if res.Username == user.Username && res.Password == user.Password {
		userSession.UserID = res.ID
		userSession.Token = token
		_, err := db.client.Database("coding_challenge").Collection("sessions").InsertOne(ctx, userSession)
		return userSession, err
	}
	return Session{}, errors.New("no user found")
}

func (db *DB) getCoupons(ctx context.Context) ([]Coupon, error) {
	var coupons []Coupon

	cur, err := db.client.Database("coding_challenge").Collection("coupons").Find(ctx, bson.M{})
	if err != nil {
		return []Coupon{}, err
	}

	for cur.Next(ctx) {
		var elem Coupon
		err := cur.Decode(&elem)
		if err != nil {
			return []Coupon{}, err
		}
		coupons = append(coupons, elem)
	}

	return coupons, errors.New("no user found")
}

func (db *DB) addCoupons(ctx context.Context, coupon Coupon) error {

	_, err := db.client.Database("coding_challenge").Collection("coupons").InsertOne(ctx, coupon)
	if err != nil {
		return err
	}

	return nil
}

func (db *DB) checkToken(ctx context.Context, token string) error {
	var res Session

	if err := db.client.Database("coding_challenge").Collection("sessions").FindOne(ctx, bson.M{"token": token}).Decode(&res); err != nil {
		return err
	}
	if res.Token != token {
		return errors.New("token invalid")
	}

	return nil
}




func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With, token")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func main() {
	router := gin.Default()
	router.Use(CORSMiddleware())
	router.POST("/register", register)
	router.POST("/login", login)

	router.GET("/coupons", couponsGet)
	router.POST("/coupons", couponsPost)

	//router.Run(":8080")
	router.Run(":8000")
}



//Endpoints functions
func register(ctx *gin.Context) {
	db, err := NewDB(ctx.Request.Context(), "mongodb://localhost:27017")
	if err != nil {
		ctx.String(http.StatusInternalServerError, err.Error())
		return
	}
	var reqBody User

	if err := ctx.BindJSON(&reqBody); err != nil {
		ctx.String(http.StatusInternalServerError, err.Error())
		return
	}
	err = db.register(ctx.Request.Context(), reqBody)
	if err != nil {
		ctx.JSON(http.StatusOK, map[string]string{"error": err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, map[string]string{
		"status": "OK",
	})
}

func login(ctx *gin.Context) {
	db, err := NewDB(ctx.Request.Context(), "mongodb://localhost:27017")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	var reqBody User
	var userSession Session
	if err := ctx.BindJSON(&reqBody); err != nil {
		ctx.JSON(http.StatusOK, map[string]string{"error": err.Error()})
		return
	}
	userSession, err = db.login(ctx.Request.Context(), reqBody, GenerateSecureToken(32))
	if err != nil {
		ctx.JSON(http.StatusForbidden, map[string]string{"error": err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, map[string]string{
		"token": userSession.Token,
	})
}

func couponsPost(ctx *gin.Context) {
	db, err := NewDB(ctx.Request.Context(), "mongodb://localhost:27017")
	if err != nil {
		return
	}
	var reqBody Coupon

	if err := ctx.BindJSON(&reqBody); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if db.checkToken(ctx, ctx.GetHeader("token")) != nil {
		ctx.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
		return
	}
	log.Print(reqBody)

	db.addCoupons(ctx.Request.Context(), reqBody)
	coupons, err := db.getCoupons(ctx.Request.Context())
	ctx.JSON(http.StatusOK, coupons)
}

func couponsGet(ctx *gin.Context) {
	db, err := NewDB(ctx.Request.Context(), "mongodb://localhost:27017")
	if err != nil {
		return
	}
	if db.checkToken(ctx, ctx.GetHeader("token")) != nil {
		ctx.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
		return
	}
	coupons, err := db.getCoupons(ctx.Request.Context())
	ctx.JSON(http.StatusOK, coupons)
}


//Database fabricator
func NewDB(ctx context.Context, uri string) (*DB, error) {
	clientOptions := options.Client().ApplyURI(uri)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, err
	}
	db := DB{
		client: client,
	}
	return &db, nil
}


//User session tokens generation
func GenerateSecureToken(length int) string {
	b := make([]byte, length)
	if _, err := rand.Read(b); err != nil {
		return ""
	}
	return hex.EncodeToString(b)
}
